package com.example.demo.enity;

import lombok.Data;

/**
 *@author zs
 *@date 2020/1/20 16:53
 *@description
 */
@Data
public class MyDate {
    private Integer id;

    private String status;
}
