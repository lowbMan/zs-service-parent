package com.example.demo.mybatis;

public interface UserDao {

    Integer queryUserById(Integer id);
}
