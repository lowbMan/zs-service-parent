package com.example.demo.mybatis;

public interface SqlSessionFactory {

    public SqlSession openSession();
}
