package com.example.demo.mybatis;

public interface UserMapper {
    Integer findUserById(Integer id);
}
