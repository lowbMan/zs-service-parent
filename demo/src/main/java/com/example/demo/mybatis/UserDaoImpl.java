package com.example.demo.mybatis;

public class UserDaoImpl implements UserDao{


    private SqlSessionFactory sqlSessionFactory;

    public  UserDaoImpl(SqlSessionFactory sqlSessionFactory){
        sqlSessionFactory=sqlSessionFactory;
    }

    @Override
    public Integer queryUserById(Integer id) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        String statementId="test.findUserById";

        Integer data = sqlSession.selectOne(statementId, id);

        return data;
    }
}
