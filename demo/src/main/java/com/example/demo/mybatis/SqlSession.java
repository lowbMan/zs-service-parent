package com.example.demo.mybatis;

import java.util.List;

public interface SqlSession {

    public <T>T getMapper(Class<T> clazz);

    <T>T selectOne(String statementId,Object param);

    <T>List<T> selectList(String statementId,Object param);
}
