package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.enity.MyDate;
import org.apache.ibatis.annotations.Mapper;

/**
 *@author zs
 *@date 2020/1/20 16:55
 *@description
 */
@Mapper
public interface MyDateMapper extends BaseMapper<MyDate> {
}
