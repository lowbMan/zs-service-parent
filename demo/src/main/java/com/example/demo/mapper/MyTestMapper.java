package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.enity.MyTest;
import org.apache.ibatis.annotations.Mapper;

/**
 *@author zs
 *@date 2020/1/20 16:54
 *@description
 */
@Mapper
public interface MyTestMapper extends BaseMapper<MyTest> {
}
