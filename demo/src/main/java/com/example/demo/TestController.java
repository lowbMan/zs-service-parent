package com.example.demo;


import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.example.demo.enity.MyDate;
import com.example.demo.enity.MyTest;
import com.example.demo.mapper.MyDateMapper;
import com.example.demo.mapper.MyTestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 *@author zs
 *@date 2020/1/3 14:56
 *@Company:成都壹柒互动科技有限公司
 *@description TODO
 */
@RestController
@RequestMapping("/test")
public class TestController {


    @Autowired
    private MyDateMapper myDateMapper;

    @Autowired
    private MyTestMapper myTestMapper;

    @GetMapping("/get33")
    public String get33(Integer num)  {
        MyTest myTest=new MyTest();
        Snowflake snowflake = IdUtil.createSnowflake(1, 1);
        myTest.setId((int) snowflake.nextId());
        myTest.setToday("3");
        myTestMapper.insert(myTest);
        return null;
    }

    @GetMapping("/get22")
    public String get22(Integer num)  {
        List<MyDate> myDates = myDateMapper.selectList(null);
        System.out.println(myDates);
        return null;
    }
}
