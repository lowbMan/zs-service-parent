package com.example.demo;

import com.example.demo.mybatis.SqlSessionFacotryBuilder;
import com.example.demo.mybatis.SqlSessionFactory;
import com.example.demo.mybatis.UserDao;
import com.example.demo.mybatis.UserDaoImpl;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.InputStream;

@SpringBootTest
class DemoApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void test1() {
        String resource="UserMapper.xml";
        InputStream in=this.getClass().getClassLoader().getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory=new SqlSessionFacotryBuilder().build(in);
        UserDao userDao=new UserDaoImpl(sqlSessionFactory);
        Integer integer = userDao.queryUserById(1);
        System.out.println(integer);
    }

}
