package com.example.transaction.chamada.demo;

/**
 *@author zs
 *@date 2020/1/17 16:44
 *@Company:成都壹柒互动科技有限公司
 *@description TODO
 */
public interface Listener {
    void result(Object result);
}
