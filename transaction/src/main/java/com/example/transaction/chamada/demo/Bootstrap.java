package com.example.transaction.chamada.demo;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 *@author zs
 *@date 2020/1/17 16:45
 *@Company:成都壹柒互动科技有限公司
 *@description TODO
 */
public class Bootstrap {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Bootstrap bootstrap=new Bootstrap();
        Worker worker = bootstrap.newWorker();
        Wrapper wrapper=new Wrapper();
        wrapper.setWorker(worker);
        wrapper.setParam("hello");




    }


    private Wrapper doWork(Wrapper wrapper){
        new Thread(()->{
            Worker worker=wrapper.getWorker();
            String result=worker.action(wrapper.getParam());
            wrapper.getListener().result(result);
        }).start();
        return wrapper;
    }

    private Worker newWorker(){
        return new Worker() {
            @Override
            public String action(Object object) {
                try{
                    Thread.sleep(1000);
                }catch (Exception e){
                    e.printStackTrace();
                }
                return object + "world";
            }
        };
    }
}
