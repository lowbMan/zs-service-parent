package com.example.transaction.chamada.demo;

/**
 *@author zs
 *@date 2020/1/17 16:44
 *@Company:成都壹柒互动科技有限公司
 *@description TODO
 */
public class Wrapper {
    private Object param;
    private Worker worker;
    private Listener listener;

    public Object getParam() {
        return param;
    }

    public void setParam(Object param) {
        this.param = param;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    public Listener getListener() {
        return listener;
    }

    public void addListener(Listener listener) {
        this.listener = listener;
    }
}
