package com.example.transaction.chamada.demo;

/**
 *@author zs
 *@date 2020/1/17 16:42
 *@description TODO
 */
public interface Worker {
    String action(Object object);
}
