package com.example.transaction.service.impl;

import com.example.transaction.enity.Project;
import com.example.transaction.mapper.ProjectMapper;
import com.example.transaction.mapper.UserMapper;
import com.example.transaction.service.TestService;
import com.example.transaction.util.RedissLockUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 *@author zs
 *@date 2019/10/25 17:07
 *@Company:成都壹柒互动科技有限公司
 *@description TODO
 */
@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private ProjectMapper projectMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public void test() {

        boolean res=false;
        try{
            res = RedissLockUtil.tryLock("123121312"+"", TimeUnit.SECONDS, 3, 20);
            if(res){
                //查看库存够不够
                Project select = projectMapper.selectById(1);
                if(select.getNumber()>0){
                    //库存-1
                    Project project=new Project();
                    projectMapper.updateProject(project);

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(res){
                //释放锁
                RedissLockUtil.unlock("123121312"+"");
            }
        }


    }


}
