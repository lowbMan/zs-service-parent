package com.example.transaction.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.transaction.enity.MyTest;
import com.example.transaction.enity.Project;
import org.apache.ibatis.annotations.Mapper;

/**
 *@author zs
 *@date 2020/1/20 16:54
 *@description
 */
@Mapper
public interface MyTestMapper extends BaseMapper<MyTest> {
}
