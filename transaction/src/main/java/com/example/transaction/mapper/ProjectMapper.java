package com.example.transaction.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.transaction.enity.Project;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2019-10-25
 */
@Mapper
public interface ProjectMapper extends BaseMapper<Project> {

    Integer updateProject(Project project);
}
