package com.example.transaction.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.transaction.enity.MyDate;
import com.example.transaction.enity.MyTest;
import org.apache.ibatis.annotations.Mapper;

/**
 *@author zs
 *@date 2020/1/20 16:55
 *@description
 */
@Mapper
public interface MyDateMapper extends BaseMapper<MyDate> {
}
