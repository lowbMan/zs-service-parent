package com.example.transaction.disruptor;

import com.lmax.disruptor.EventFactory;
import javafx.event.EventType;

/**
 *@author zs
 *@date 2019/12/28 14:16
 *@Company:成都壹柒互动科技有限公司
 *@description TODO
 */
public class LongEventFactory implements EventFactory<LongEvent> {

    @Override
    public LongEvent newInstance()
    {
        return new LongEvent();
    }
}
