package com.example.transaction.disruptor;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.WorkHandler;
import lombok.extern.slf4j.Slf4j;


/**
 *@author zs
 *@date 2019/12/28 14:19
 *@Company:成都壹柒互动科技有限公司
 *@description TODO
 */
@Slf4j
public class LongEventHandler implements EventHandler<LongEvent> {

    @Override
    public void onEvent(LongEvent event, long sequence, boolean endOfBatch)
    {
        log.info(event.toString());
    }

}
