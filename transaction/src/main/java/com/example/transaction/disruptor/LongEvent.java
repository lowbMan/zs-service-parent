package com.example.transaction.disruptor;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 *@author zs
 *@date 2019/12/28 14:16
 *@Company:成都壹柒互动科技有限公司
 *@description TODO
 */
public class LongEvent  {
    private long value;

    public void set(long value)
    {
        this.value = value;
    }

    @Override
    public String toString() {
        return "LongEvent{" +
                "value=" + value +
                '}';
    }
}
