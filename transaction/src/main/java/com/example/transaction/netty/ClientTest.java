package com.example.transaction.netty;

import com.alipay.api.java_websocket.WebSocket;
import com.alipay.api.java_websocket.drafts.Draft_6455;


import java.net.URI;
import java.net.URISyntaxException;

/**
 *@author zs
 *@date 2020/1/16 15:41
 *@Company:成都壹柒互动科技有限公司
 *@description TODO
 */
public class ClientTest {
    public static void main(String[] args) throws URISyntaxException, InterruptedException {
        MyWebSocketClient client = new MyWebSocketClient( new URI("ws://127.0.0.1:6655/ws"));
        client.connect();

        //连接成功,发送信息
        client.send("哈喽,连接一下啊");
    }
}
