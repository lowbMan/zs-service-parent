package com.example.transaction.netty;

import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

/**
 *@author zs
 *@date 2020/1/16 15:18
 *@description TODO 通道组池，管理所有websocket连接
 */
public class MyChannelHandlerPool {

    public static ChannelGroup channelGroup=new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    public MyChannelHandlerPool(){}


}
