package com.example.transaction.netty;

import com.alipay.api.java_websocket.client.WebSocketClient;
import com.alipay.api.java_websocket.handshake.ServerHandshake;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.drafts.Draft;

import java.net.URI;

/**
 *@author zs
 *@date 2020/1/16 15:35
 *@description TODO
 */
@Slf4j
public class MyWebSocketClient extends org.java_websocket.client.WebSocketClient {


    public MyWebSocketClient(URI serverUri) {
        super(serverUri);
    }

    @Override
    public void onOpen(org.java_websocket.handshake.ServerHandshake serverHandshake) {
        System.out.println("开始建立链接...");
    }

    @Override
    public void onMessage(String s) {
        System.out.println("检测到服务器请求...message： " + s);
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        System.out.println("客户端已关闭!");
    }

    @Override
    public void onError(Exception e) {
        e.printStackTrace();
        System.out.println("客户端发生错误,即将关闭!");
    }
}
