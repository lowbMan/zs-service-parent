package com.example.transaction;


import cn.hutool.core.lang.Snowflake;

import cn.hutool.core.util.IdUtil;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeWapPayResponse;
import com.example.transaction.enity.MyTest;
import com.example.transaction.enity.Project;
import com.example.transaction.enity.User;
import com.example.transaction.mapper.MyDateMapper;
import com.example.transaction.mapper.MyTestMapper;
import com.example.transaction.mapper.ProjectMapper;
import com.example.transaction.mapper.UserMapper;
import com.example.transaction.service.TestService;
import com.example.transaction.util.CircleArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *@author zs
 *@date 2019/10/25 16:20
 *@Company:成都壹柒互动科技有限公司
 *@description TODO
 */
@RestController
@RequestMapping("/test")
public class TestController {

    private static int corePoolSize = Runtime.getRuntime().availableProcessors();
    //调整队列数 拒绝服务
    private static ThreadPoolExecutor executor  = new ThreadPoolExecutor(corePoolSize, corePoolSize+1, 10l, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(10000));

    @Autowired
    private TestService testService;

    @Autowired
    private MyDateMapper myDateMapper;

    @Autowired
    private MyTestMapper myTestMapper;

    @RequestMapping("/get")
    public String get(Integer num) {
        //jdk 8 新特性 非空判断
        Integer integer = Optional.ofNullable(num).orElse(2);
        try{
            //睡5秒，网关Hystrix3秒超时，会触发熔断降级操作
            Thread.sleep(5000);
        }catch (Exception e){
            e.printStackTrace();
        }
        return integer*2+"";
    }

    @RequestMapping("/get33")
    public String get33(Integer num) throws AlipayApiException {
        MyTest myTest=new MyTest();
        Snowflake snowflake = IdUtil.createSnowflake(1, 1);
        myTest.setId(snowflake.nextId());
        myTest.setToday("2");
        myTestMapper.insert(myTest);
        return null;
    }




    @RequestMapping("/get22")
    public String get22(Integer num) throws AlipayApiException {
        AlipayClient alipayClient = new DefaultAlipayClient(
                "https://openapi.alipaydev.com/gateway.do",
                "2016101300673909",
                "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCt2Ceq60y4mgY4f3nflXwpPNZ9AIFxwh9lBWodgGKsqISpeye6UayqYM/iCq1OPvtSa7chofiyz7P+en47pN2rePyAkau/F23e39lAKR1WQaMU43JiUAG5EbiQdG+6TACzBVRlyAkaFqnzVDWAD89KcsiFbXBTKWVaVooS5qWCoP/BHfovUbStwyzsCAHX89v96zhr1zEvt8zTByC0eqxckaSNYTfcYSKwbGTKcna1da7THRN9ZT+WLizz4i+OzwPYRwVB6DC87BcspXlHz7tjE3k7n3hhTTUqrEvmUUEjQEeCgfbooXYpjuFyH650j4s9kf1MqmScL+Uya2aGxYc/AgMBAAECggEALq2UcRs+/W4172OjGLqtnPD98pB5MU9bWKesh4FafVu+SzeLQkb94r2d8+bEA/d7SfUsNfakrXaBsPm4/VDWSF1zBx6WC/hBgw996PFhNDDplglARE+RNt0gPoFVIqOXqHns4Gtmrj+ZoIFi4CDemvHZD6KD/4Bk+22zI0IU/M7Gng1+e1Rf+aP8lLrG0+jB0fuQxJ8WZ/NVxydH/w1jmSPnhX/tR4rKRsVQ2dJOsNNl57bcB9YmaNDMRL0VFQZsqL1Xb80u2T28MvVVn2R5ETw278U18P6CuYL7GMPmyeMnecVDbqpvOkaeb2pTUdoHv3Oi7DWemt84XPB0IiOscQKBgQDbL5voWmUU3YZPt005Aj8JK4hTC5ZxdEmDQj0xivWHx72Nxu7Rmyr15hgoh3YzzdsCRItj/IEynfebIEzrghPnlIE1YBqOF+lpktE0R1lpcV2YwHA4XHPevTEcANoWD/h37/DflFz5MEf6jijj7CRjn+WcH/+PrxsjUQEewn4AhwKBgQDLCvtRpDwFwCX2YMaLscOyCNkhdMv1LkLshkFXbCElPix1QhVPd0GVS2d84a+/DgneKepUcBXYO9uoiuWAEqxzYBj3KM1+tHsg7USzuug5Rm77d8N/anWwN5mVW3p2bBFANCeK/Hy7ZzQRCQzZg8IVoK3fYUs5tQBX2lU+MzmJiQKBgQCXDlKWaWXsYVclLSCtP10OBtBm2z+NZSUcrtPZhvj74K5EPWttTVDoMvFHTUQrI53iDDc8a1Lm/XMdl78rN3RcTz5U+KyUbOpR9frz2pBGWPi2QzmTvjOkuCJWOEgUmGEW12fbzjXdDmcsyDhqC95wQkxNsse/0GAVkMgC2OtZlQKBgAegq/xPLSoSI9rzm8HXtcBUcMO+/YKCcGUuTfoYx838XfMxvlcz5bTUFlksqUuGHXzJGlJ9u3RaG1Bjl2eOTAgxjf6K+91Mrmt4PmJ6HQs5yCWORDvfJfiuvghMXXdha+x7iRjBj3YC1IPnUSJCvuHNHXsMzqSmVhL20sj5Z6nxAoGASX4lBH/FORPxw6NcExghYP+kxNAjndeD2M2VFNm+OpK5LqvqqNE9yXd1nbhL4Ga8LzbtTddQeUReqgoaVg417LDt6QevwR0A0HT0qxp7t2QIrbm9hw0AY09BZjp38pbm656UXmVq/EnQ6q7zXBkeUwACVC0lJ0VhwX1R05xE7M4=",
                "json",
                "GBK",
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAloiMTb61tgdxHQ0uOWrm+WS2TDTfPu/X7nHQE5ETdADGgWtZy06swteLlsmVF8FF8/Q2D4xAhBt5qCtJB+JmyX5eCuoBusxo+aP1Q",
                "RSA2");
        AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
        request.setBizContent("{" +
                "\"body\":\"Iphone6 16G\"," +
                "\"subject\":\"大乐透\"," +
                "\"out_trade_no\":\"70501111111S001111119\"," +
                "\"timeout_express\":\"90m\"," +
                "\"time_expire\":\"2016-12-31 10:05\"," +
                "\"total_amount\":9.00," +
                "\"seller_id\":\"2088102147948060\"," +
                "\"auth_token\":\"appopenBb64d181d0146481ab6a762c00714cC27\"," +
                "\"goods_type\":\"0\"," +
                "\"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                "\"quit_url\":\"http://www.taobao.com/product/113714.html\"," +
                "\"product_code\":\"QUICK_WAP_WAY\"," +
                "\"promo_params\":\"{\\\"storeIdType\\\":\\\"1\\\"}\"," +
                "\"royalty_info\":{" +
                "\"royalty_type\":\"ROYALTY\"," +
                "        \"royalty_detail_infos\":[{" +
                "          \"serial_no\":1," +
                "\"trans_in_type\":\"userId\"," +
                "\"batch_no\":\"123\"," +
                "\"out_relation_id\":\"20131124001\"," +
                "\"trans_out_type\":\"userId\"," +
                "\"trans_out\":\"2088101126765726\"," +
                "\"trans_in\":\"2088101126708402\"," +
                "\"amount\":0.1," +
                "\"desc\":\"分账测试1\"," +
                "\"amount_percentage\":\"100\"" +
                "          }]" +
                "    }," +
                "\"extend_params\":{" +
                "\"sys_service_provider_id\":\"2088511833207846\"," +
                "\"hb_fq_num\":\"3\"," +
                "\"hb_fq_seller_percent\":\"100\"," +
                "\"industry_reflux_info\":\"{\\\\\\\"scene_code\\\\\\\":\\\\\\\"metro_tradeorder\\\\\\\",\\\\\\\"channel\\\\\\\":\\\\\\\"xxxx\\\\\\\",\\\\\\\"scene_data\\\\\\\":{\\\\\\\"asset_name\\\\\\\":\\\\\\\"ALIPAY\\\\\\\"}}\"," +
                "\"card_type\":\"S0JP0000\"" +
                "    }," +
                "\"sub_merchant\":{" +
                "\"merchant_id\":\"19023454\"," +
                "\"merchant_type\":\"alipay: 支付宝分配的间连商户编号, merchant: 商户端的间连商户编号\"" +
                "    }," +
                "\"merchant_order_no\":\"20161008001\"," +
                "\"enable_pay_channels\":\"pcredit,moneyFund,debitCardExpress\"," +
                "\"disable_pay_channels\":\"pcredit,moneyFund,debitCardExpress\"," +
                "\"store_id\":\"NJ_001\"," +
                "\"settle_info\":{" +
                "        \"settle_detail_infos\":[{" +
                "          \"trans_in_type\":\"cardAliasNo\"," +
                "\"trans_in\":\"A0001\"," +
                "\"summary_dimension\":\"A0001\"," +
                "\"settle_entity_id\":\"2088xxxxx;ST_0001\"," +
                "\"settle_entity_type\":\"SecondMerchant、Store\"," +
                "\"amount\":0.1" +
                "          }]" +
                "    }," +
                "\"invoice_info\":{" +
                "\"key_info\":{" +
                "\"is_support_invoice\":true," +
                "\"invoice_merchant_name\":\"ABC|003\"," +
                "\"tax_num\":\"1464888883494\"" +
                "      }," +
                "\"details\":\"[{\\\"code\\\":\\\"100294400\\\",\\\"name\\\":\\\"服饰\\\",\\\"num\\\":\\\"2\\\",\\\"sumPrice\\\":\\\"200.00\\\",\\\"taxRate\\\":\\\"6%\\\"}]\"" +
                "    }," +
                "\"specified_channel\":\"pcredit\"," +
                "\"business_params\":\"{\\\"data\\\":\\\"123\\\"}\"," +
                "\"ext_user_info\":{" +
                "\"name\":\"李明\"," +
                "\"mobile\":\"16587658765\"," +
                "\"cert_type\":\"IDENTITY_CARD\"," +
                "\"cert_no\":\"362334768769238881\"," +
                "\"min_age\":\"18\"," +
                "\"fix_buyer\":\"F\"," +
                "\"need_check_info\":\"F\"" +
                "    }" +
                "  }");
        AlipayTradeWapPayResponse response = alipayClient.pageExecute(request);
        if(response.isSuccess()){
            String body = response.getBody();
            return body;
        } else {
            System.out.println("调用失败");
            return null;
        }
    }
    public static void main(String[] args) {
        //测试一波啦
        System.out.println("测试一个模拟环形队列的案例~~");
        //创建一个环形队列
        CircleArray queue=new CircleArray(4);//说明设置4，其队列最大是3
        char key=' ';//接收用户输入
        Scanner scanner=new Scanner(System.in);
        boolean loop=true;
        //输出一个菜单
        while(loop) {
            System.out.println("s(show):显示队列");
            System.out.println("e(sxit):退出程序");
            System.out.println("a(add):添加数据到队列");
            System.out.println("g(get):从队列取出数据");
            System.out.println("h(head):查看队头的数据");
            key=scanner.next().charAt(0);//接收一个字符
            switch(key) {
                case 's':
                    queue.showQueue();
                    break;
                case 'a':
                    System.out.println("输出一个数");
                    int value=scanner.nextInt();
                    queue.addQueue(value);
                    break;
                case 'g'://取出数据
                    try {
                        int res=queue.getQueue();
                        System.out.println("取出的数据是"+res);

                    }catch (Exception e) {
                        System.out.println(e.getMessage());//输出异常信息
                    }
                    break;
                case 'h':
                    try {
                        int res=queue.headQueue();
                        System.out.println("队列头的数据"+res);

                    }catch (Exception e) {
                        System.out.println(e.getMessage());//输出异常信息
                    }
                    break;
                case 'e'://退出
                    scanner.close();
                    loop=false;
                    break;
                default:
                    break;
            }
        }
        System.out.println("程序退出");
    }
}
