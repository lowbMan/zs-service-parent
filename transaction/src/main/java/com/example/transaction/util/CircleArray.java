package com.example.transaction.util;

/**
 *@author zs
 *@date 2019/12/28 14:06
 *@Company:成都壹柒互动科技有限公司
 *@description TODO 环形队列
 */
public class CircleArray {

    private int maxSize;//表示数组的最大容量
    //front变量的含义做一个调整：front就是指向队列的第一个元素，也就是说arr【front】
    //front的初值=0
    private int front;
    //rear变量的含义做一个调整：rear指向队列的最后一个元素的一个位置，因为希望空出一个
    //rear的初始值=0
    private int rear;
    private int[] arr;//该数组用于存放数据，模拟队列

    public CircleArray(int arrMaxSize) {
        maxSize=arrMaxSize;
        arr=new int[maxSize];
    }

    //判断队列是否满
    public boolean isFull() {
        return (rear+1)%maxSize==front;
    }

    //判断队列是否为空
    public boolean isEmpty() {
        return rear==front;
    }

    //添加数据到队列
    public void addQueue(int n) {
        //判断队列是否满
        if(isFull()) {
            System.out.println("队列满，不能加入数据~~");
            return;
        }
        //直接将数据加入
        arr[rear]=n;
        rear=(rear+1)%maxSize;
    }

    //数据出队列
    public int getQueue() {
        //判断队列是否为空
        if(isEmpty()) {
            //通过抛出异常
            throw new RuntimeException("队列空，不能取数据");
        }
        //这里需要分析出front始指向队列的第一个元素
        //1.先把front对应的值保存到一个临时变量
        //2.front后移
        //3.将临时保存的变量返回
        int value=arr[front];
        front=(front+1)%maxSize;
        return value;
    }

    //显示队列的所有数据
    public void showQueue() {
        //遍历
        if(isEmpty()) {
            System.out.println("队列空的，没有数据~~~");
            return;
        }
        //思路：从front开始遍历，遍历多少个元素就可以
        for(int i=front;i<front+size();i++) {
            System.out.printf("arr[%d]=%d\n",i%maxSize,arr[i]);
        }
    }

    //求出当前队列有效的数据的个数
    public int size() {
        //rear=1;
        //front=1;
        //maxSize=2;
        return (rear+maxSize-front)%maxSize;
    }

    //显示队列的头部数据，不是取出数据
    public int headQueue() {
        if(isEmpty()) {
            throw new RuntimeException("队列为空，没有数据~~");
        }
        return arr[front];
    }

}
