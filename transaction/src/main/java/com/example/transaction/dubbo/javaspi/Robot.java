package com.example.transaction.dubbo.javaspi;

import org.apache.dubbo.common.extension.SPI;

/**
 *@author zs
 *@date 2020/1/18 14:48
 *@description TODO
 */
@SPI
public interface Robot {
    void sayHello();
}
