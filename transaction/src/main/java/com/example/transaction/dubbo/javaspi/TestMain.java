package com.example.transaction.dubbo.javaspi;

import org.apache.dubbo.common.extension.ExtensionLoader;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.ServiceLoader;

/**
 *@author zs
 *@date 2020/1/18 14:53
 *@description
 */
public class TestMain {
    public static void main(String[] args) throws IOException {
        /**
         * java spi
         */
//        ServiceLoader<Robot> serviceLoader=ServiceLoader.load(Robot.class);
//        System.out.println("Java SPI");
//        serviceLoader.forEach(Robot::sayHello);
        /**
         * dubbo spi
         */
        ExtensionLoader<Robot> extensionLoader=ExtensionLoader.getExtensionLoader(Robot.class);
        Robot optimnusPrime = extensionLoader.getExtension("optimnusPrime");
        optimnusPrime.sayHello();
        Robot bumblebee = extensionLoader.getExtension("bumblebee");
        bumblebee.sayHello();

    }
}
