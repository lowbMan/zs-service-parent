package com.example.transaction.dubbo.javaspi;

/**
 *@author zs
 *@date 2020/1/18 14:50
 *@description
 */
public class Bumblebee implements Robot{
    @Override
    public void sayHello() {
        System.out.println("Hello,I am Bumblebee");
    }
}
