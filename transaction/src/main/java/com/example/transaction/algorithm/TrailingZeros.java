package com.example.transaction.algorithm;


import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeWapPayResponse;

/**
 *@author zs
 *@date 2019/12/28 16:38
 *@description TODO 给出n的阶层 计算末尾0的个数
 */
public class TrailingZeros {
    public static void main(String[] args) throws AlipayApiException {
        long n=11;
        long c=0;
        while(n>0){
            c=c+n/5;
            n=n/5;
        }

        System.out.println(c);



    }
}
