package com.example.transaction.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 *@author zs
 *@date 2020/1/8 17:30

 */
public class MinStack {

    private List<Integer> list=new ArrayList<>();

    public MinStack() {
        // do intialization if necessary
    }

    /**
     * @param number: An integer
     * @return: nothing
     */
    public void push(int number) {
        // write your code here
        list.add(number);
    }

    /**
     * @return: An integer
     */
    public int pop() {
        // write your code here
        Integer integer = list.get(list.size() - 1);
        list.remove(list.size()-1);
        return integer;
    }

    /**
     * @return: An integer
     */
    public int min() {
        // write your code here
        Integer temp=list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if (temp>list.get(i)){
                temp=list.get(i);
            }
        }
        return temp;
    }
}
