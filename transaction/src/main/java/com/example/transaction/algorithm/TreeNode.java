package com.example.transaction.algorithm;

import lombok.Data;

/**
 *@author zs
 *@date 2020/1/3 11:16
 *@Company:成都壹柒互动科技有限公司
 *@description TODO 二叉树实体类
 */
@Data
public class TreeNode {
    public int val;
    public TreeNode left, right;

    public TreeNode(int val) {
        this.val = val;
        this.left = this.right = null;
    }


}
