package com.example.transaction.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 *@author zs
 *@date 2020/1/8 15:53
 * 描述
 * 中文
 * English
 * 给定一个二叉查找树和范围[k1, k2]。按照升序返回给定范围内的节点值。
 *
 * 您在真实的面试中是否遇到过这个题？
 * 样例
 * 样例 1:
 *
 * 输入：{5},6,10
 * 输出：[]
 *         5
 * 它将被序列化为 {5}
 * 没有数字介于6和10之间
 * 样例 2:
 *
 * 输入：{20,8,22,4,12},10,22
 * 输出：[12,20,22]
 * 解释：
 *         20
 *        /  \
 *       8   22
 *      / \
 *     4   12
 * 它将被序列化为 {20,8,22,4,12}
 * [12,20,22]介于10和22之间
 */
public class BinarySearchTree {
    public static void main(String[] args) {
        TreeNode deserialize = deserialize("{20,1,40,#,#,35}");
        List<Integer> integerList = searchRange(deserialize, 0, 0);
        for (int i = 0; i < integerList.size(); i++) {
            System.out.println(integerList.get(i));
        }
    }

    /**
     * @param root: param root: The root of the binary search tree
     * @param k1: An integer
     * @param k2: An integer
     * @return: return: Return all keys that k1<=key<=k2 in ascending order
     */
    public static List<Integer> searchRange(TreeNode root, int k1, int k2) {
        // write your code here
        if(root==null){
            return new ArrayList<>();
        }
        List<Integer> list=new ArrayList<>();
        ergodic(list,root);
        List<Integer> resultList=new ArrayList<>();
//        for (int i = 0; i < list.size(); i++) {
//            if (list.get(i)>=k1 && list.get(i)<=k2){
//                resultList.add(list.get(i));
//            }
//        }
        return list;
    }

    public static void ergodic(List<Integer> list,TreeNode node){
        if(node.left!=null){
            ergodic(list,node.left);
        }
        list.add(node.val);
        if (node.right!=null){
            ergodic(list,node.right);
        }
    }


    public static TreeNode deserialize(String data) {
        // write your code here
        if ("{}".equals(data)){
            return null;
        }
        String[] strings = data.substring(1,data.length()-1).split(",");
        TreeNode treeNode=new TreeNode(Integer.parseInt(strings[0]));
        List<TreeNode> list=new ArrayList<>();
        list.add(treeNode);
        //判断是左还是右 0--左 1--右
        int flag=0;
        int index=0;
        for (int i=1;i<strings.length;i++){
            if ("#".equals(strings[i])){
                if(flag==0){
                    flag=1;
                }else if (flag==1){
                    flag=0;
                    index++;
                }
            }else {
                TreeNode node = new TreeNode(Integer.parseInt(strings[i]));
                TreeNode start = list.get(index);
                list.add(node);
                if(flag==0){
                    start.left=node;
                    flag=1;
                }else {
                    start.right=node;
                    flag=0;
                    index++;
                }
            }
        }
        return treeNode;

    }
}
