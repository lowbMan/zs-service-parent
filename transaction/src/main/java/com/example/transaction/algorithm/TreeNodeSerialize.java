package com.example.transaction.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 *@author zs
 *@date 2020/1/3 11:17
 * 设计一个算法，并编写代码来序列化和反序列化二叉树。将树写入一个文件被称为“序列化”，读取文件后重建同样的二叉树被称为“反序列化”。
 *
 * 如何反序列化或序列化二叉树是没有限制的，你只需要确保可以将二叉树序列化为一个字符串，并且可以将字符串反序列化为原来的树结构。
 *
 * 样例
 * 样例 1：
 *
 * 输入：{3,9,20,#,#,15,7}
 * 输出：{3,9,20,#,#,15,7}
 * 解释：
 * 二叉树 {3,9,20,#,#,15,7}，表示如下的树结构：
 * 	  3
 * 	 / \
 * 	9  20
 * 	  /  \
 * 	 15   7
 * 它将被序列化为 {3,9,20,#,#,15,7}
 * 样例 2：
 *
 * 输入：{1,2,3}
 * 输出：{1,2,3}
 * 解释：
 * 二叉树 {1,2,3}，表示如下的树结构：
 *    1
 *   / \
 *  2   3
 * 它将被序列化为 {1,2,3}
 */
public class TreeNodeSerialize {
    public static void main(String[] args) {

    }

    /**
     * This method will be invoked first, you should design your own algorithm
     * to serialize a binary tree which denote by a root node to a string which
     * can be easily deserialized by your own "deserialize" method later.
     */
    public static String serialize(TreeNode root) {
        // write your code here
        //先判断是否为空
        if(root==null){
            return "{}";
        }
        //创建数组 来添加节点得值
        List<TreeNode> list=new ArrayList<>();
        //首先添加根节点得值
        list.add(root);
        //遍历 list 并添加子节点值 该循环是整个方法得核心
        for (int i=0;i<list.size();i++){
            if(list.get(i)==null){
                continue;
            }
            list.add(list.get(i).left);
            list.add(list.get(i).right);
        }
        //由于上一个循环在数组末尾添加了一些null 节点 需要删除掉
        while (list.get(list.size()-1)==null){
            list.remove(list.size()-1);
        }
        //再通过循环进行拼接字符串
        //这里使用StringBuilder 进行优化效率
        StringBuilder sb=new StringBuilder();
        sb.append("{");
        sb.append(list.get(0).val);
        for (int i=1;i<list.size();i++) {
            if(list.get(i)==null){
                sb.append(",#");
            }else {
                sb.append(","+list.get(i).val);
            }
        }
        sb.append("}");
        return sb.toString();
    }

    /**
     * This method will be invoked second, the argument data is what exactly
     * you serialized at method "serialize", that means the data is not given by
     * system, it's given by your own serialize method. So the format of data is
     * designed by yourself, and deserialize it here as you serialize it in
     * "serialize" method.
     */
    public static TreeNode deserialize(String data) {
        // write your code here
        if ("{}".equals(data)){
            return null;
        }
        String[] strings = data.substring(1,data.length()-1).split(",");
        TreeNode treeNode=new TreeNode(Integer.parseInt(strings[0]));
        List<TreeNode> list=new ArrayList<>();
        list.add(treeNode);
        //判断是左还是右 0--左 1--右
        int flag=0;
        int index=0;
        for (int i=1;i<strings.length;i++){
            if ("#".equals(strings[i])){
                if(flag==0){
                    flag=1;
                }else if (flag==1){
                    flag=0;
                    index++;
                }
            }else {
                TreeNode node = new TreeNode(Integer.parseInt(strings[i]));
                TreeNode start = list.get(index);
                list.add(node);
                if(flag==0){
                    start.left=node;
                    flag=1;
                }else {
                    start.right=node;
                    flag=0;
                    index++;
                }
            }
        }
        return treeNode;

    }



}
