package com.example.transaction.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 *@author zs
 *@date 2020/1/2 11:34
 *设计一个算法，找出只含素因子2，3，5 的第 n 小的数。
 *
 * 符合条件的数如：1, 2, 3, 4, 5, 6, 8, 9, 10, 12...
 *
 * 我们可以认为 1 也是一个丑数。
 *
 * 样例 1：
 *
 * 输入：9
 * 输出：10
 */
public class UglinessNumber {
    public static void main(String[] args) {

        int[] nums = {9,3,2,4,8};
        System.out.println("快速排序结果为:" + kthLargestElement(3, nums));

//        int[]A={1,3,5};
//        int[]B={4};
//        int[] ints = mergeSortedArray(A, B);
//        //合并升序数组为
//        for (int anInt : ints) {
//            System.out.print(""+anInt+" ");
//        }


    }


    /**
     * @param n: An integer
     * @param nums: An array
     * @return: the Kth largest element
     * 在数组中找到第 k 大的元素。
     *
     * 你可以交换数组中的元素的位置
     *
     * 样例 1：
     *
     * 输入：
     * n = 1, nums = [1,3,4,2]
     * 输出：
     * 4
     * 样例 2：
     *
     * 输入：
     * n = 3, nums = [9,3,2,4,8]
     * 输出：
     * 4
     *
     * 挑战
     * 要求时间复杂度为O(n)，空间复杂度为O(1)。
     */
    public static int kthLargestElement(int n, int[] nums) {
        // write your code here
        quickSelect(0, nums.length - 1, nums, n - 1);
        return nums[n - 1];
    }

    /**
     * 快速排序
     * @param left
     * @param right
     * @param nums
     * @param n
     */
    public static void quickSelect(int start, int end, int[] nums, int n) {
        int pivot = nums[(start + end) / 2];
        int left = start;
        int right = end;
        while (left <= right) {
            while (nums[left] > pivot && left <= right) {
                left++;
            }
            while (nums[right] < pivot && left <= right) {
                right--;
            }
            if (left <= right) {
                //交换
                int swap = 0;
                swap = nums[right];
                nums[right] = nums[left];
                nums[left] = swap;
                left++;
                right--;
            }
        }
        if (right>=n) {
            quickSelect(start, right, nums, n);
        }

        if (left<n) {
            quickSelect(left, end, nums, n);
        }


    }

    /**
     * @param A: sorted integer array A
     * @param B: sorted integer array B
     * @return: A new sorted integer array
     * 合并两个有序升序的整数数组A和B变成一个新的数组。新数组也要有序
     * 样例 1:
     *
     * 输入: A=[1], B=[1]
     * 输出:[1,1]
     * 样例解释: 返回合并后的数组。
     * 样例 2:
     *
     * 输入: A=[1,2,3,4], B=[2,4,5,6]
     * 输出: [1,2,2,3,4,4,5,6]
     * 样例解释: 返回合并后的数组。
     * 挑战
     * 你能否优化你的算法，如果其中一个数组很大而另一个数组很小？
     */
    public static int[] mergeSortedArray(int[] A, int[] B) {
        // write your code here
        int[] arrays = new int[A.length+B.length];
        int right=0;
        int sub=0;
        for (int i=0;i<A.length;i++){
           if (right<=B.length-1){
               while (B[right]<A[i]){
                   arrays[sub]=B[right];
                   right++;
                   sub++;
                   if (right>B.length-1){
                       break;
                   }
               }
           }
           arrays[sub]=A[i];
           sub++;

        }
        if (A[A.length-1]<=B[B.length-1]){
            for (int i=right;i<B.length;i++){
                arrays[sub]=B[i];
                sub++;

            }
        }

        return arrays;
    }
}
