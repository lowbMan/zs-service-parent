package com.example.transaction.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 *@author zs
 *@date 2020/1/6 17:42
 * 给定一个字符串（以字符数组的形式给出）和一个偏移量，根据偏移量原地旋转字符串(从左向右旋转)。
 *
 * 说明
 * 原地旋转意味着你要在s本身进行修改。你不需要返回任何东西。
 *
 * 样例
 * 样例 1:
 *
 * 输入:  str="abcdefg", offset = 3
 * 输出:  str = "efgabcd"
 * 样例解释:  注意是原地旋转，即str旋转后为"efgabcd"
 * 样例 2:
 *
 * 输入: str="abcdefg", offset = 0
 * 输出: str = "abcdefg"
 * 样例解释: 注意是原地旋转，即str旋转后为"abcdefg"
 * 样例 3:
 *
 * 输入: str="abcdefg", offset = 1
 * 输出: str = "gabcdef"
 * 样例解释: 注意是原地旋转，即str旋转后为"gabcdef"
 * 挑战
 * 在数组上原地旋转，使用O(1)的额外空间
 */
public class RotateString {
    public static void main(String[] args) {
//        String a="abcdefg";
//        rotateString(a.toCharArray(),3);
//        System.out.println(1);
        List<String> list = fizzBuzz(15);
        for (String s : list) {
            System.out.print(s+" ");
        }
    }

    /**
     * @param str: an array of char
     * @param offset: an integer
     * @return: nothing
     */
    public static void rotateString(char[] str, int offset) {
        // write your code here
        if (str == null || str.length == 0){
            return;
        }
        offset = offset % str.length;
        reverse(str, 0, str.length - offset - 1);
        reverse(str, str.length - offset, str.length - 1);
        reverse(str, 0, str.length - 1);
    }

    private static void reverse(char[] str, int start, int end) {
        for (int i = start, j = end; i < j; i++, j--) {
            char temp = str[i];
            str[i] = str[j];
            str[j] = temp;
        }
    }

    /**
     * @param n: An integer
     * @return: A list of strings.
     * 描述
     * 中文
     * English
     * 给你一个整数n. 从 1 到 n 按照下面的规则打印每个数：
     *
     * 如果这个数被3整除，打印fizz.
     * 如果这个数被5整除，打印buzz.
     * 如果这个数能同时被3和5整除，打印fizz buzz.
     * 如果这个数既不能被 3 整除也不能被 5 整除，打印数字本身。
     * 您在真实的面试中是否遇到过这个题？
     * 样例
     * 比如 n = 15, 返回一个字符串数组：
     *
     * [
     *   "1", "2", "fizz",
     *   "4", "buzz", "fizz",
     *   "7", "8", "fizz",
     *   "buzz", "11", "fizz",
     *   "13", "14", "fizz buzz"
     * ]
     * 挑战
     * 你是否可以只用一个 if 来实现
     */
    public static List<String> fizzBuzz(int n) {
        ArrayList<String> results = new ArrayList<String>();
        int i = 1;
        //p3表示3的多少倍，p5表示5的多少倍
        int p3 = 1, p5 = 1;

        while (i <= n) {
            while (i < p3 * 3 && i < p5 * 5) {
                results.add(i + "");
                i++;
            }

            if (i <= n && p3 * 3 == p5 * 5) {
                results.add("fizz buzz");
                p3++;
                p5++;
                i++;
                continue;
            }

            while (i <= n && p3 * 3 <= i) {
                results.add("fizz");
                p3++;
                i++;
            }

            while (i <= n && p5 * 5 <= i) {
                results.add("buzz");
                p5++;
                i++;
            }
        }

        return results;
    }


}
