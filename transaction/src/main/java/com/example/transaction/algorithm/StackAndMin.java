package com.example.transaction.algorithm;

import com.sun.xml.internal.bind.v2.TODO;

import java.text.DecimalFormat;
import java.util.*;

/**
 *@author zs
 *@date 2020/1/8 17:26
 * 描述
 * 中文
 * English
 * 实现一个栈, 支持以下操作:
 *
 * push(val) 将 val 压入栈
 * pop() 将栈顶元素弹出, 并返回这个弹出的元素
 * min() 返回栈中元素的最小值
 * 要求 O(1) 开销.
 *
 * 保证栈中没有数字时不会调用 min()
 *
 * 您在真实的面试中是否遇到过这个题？
 * 样例
 * 样例 2:
 *
 * 输入:
 *   push(1)
 *   min()
 *   push(2)
 *   min()
 *   push(3)
 *   min()
 * 输出:
 *   1
 *   1
 *   1
 */
public class StackAndMin {
    public static void main(String[] args) {
//        System.out.println(strStr("source","se"));
//        int[] nums={1,2,3};
//        List<List<Integer>> subsets = subsets(nums);
//        for (List<Integer> list : subsets) {
//            for (Integer integer : list) {
//                System.out.print(integer+" ");
//            }
//            System.out.println("---");
//        }
//        quickSelect(0, nums.length - 1, nums);
//        System.out.println(nums[0]);
        isInterleave("aabcc","dbbca","aadbbcbcac");

    }

    /**
     * @param source:
     * @param target:
     * @return: return the index
     */
    public static int strStr(String source, String target) {
        // Write your code here
        char[] sourceArray = source.toCharArray();
        char[] targetArray = target.toCharArray();
        if (targetArray.length==0){
            return 0;
        }
        if (targetArray.length>sourceArray.length){
            return -1;
        }
        //利用kmp算法进行匹配
        int i=0;
        while (i<sourceArray.length){
            int flag=0;
            int j=0;
            if (i+j<sourceArray.length){
                while (sourceArray[j+i]==targetArray[j]){
                    flag++;
                    j++;
                    if (j==targetArray.length){
                        break;
                    }
                    if (i+j>=sourceArray.length){
                        break;
                    }
                }
            }
            if (flag==targetArray.length){
                return i;
            }
            if (flag==0){
                i++;
            }else {
                i=i+flag-1;
                if (i==0){
                    i++;
                }
            }

        }
        return -1;
    }

    /**
     * @param nums: The integer array.
     * @param target: Target to find.
     * @return: The first position of target. Position starts from 0.
     */
    public static int binarySearch(int[] nums, int target) {
        // write your code here
        if (nums==null || nums.length==0){
            return -1;
        }
        int start=0;
        int end=nums.length-1;
        while (start<=end){
            int index=(start+end)/2;
            if (target<nums[index]){
                //左边
                end=index-1;
            }else if (target==nums[index]){
                //找到前一个小于此值得坐标
                int flag=index;
                if (flag==0 || flag==(nums.length-1)){
                    return flag;
                }
                while (target==nums[flag]){
                    flag--;
                }
                return flag+1;
            }else {
                //右边
                start=index+1;
            }
        }
        return -1;
    }

    /**
     * @param nums: A list of integers.
     * @return: A list of permutations.
     */
    public static List<List<Integer>> permute(int[] nums) {
        // write your code here
        List<List<Integer>> lists=new ArrayList<>();
        if (nums==null || nums.length==0){
            List<Integer> list=new ArrayList<>();
            lists.add(list);
            return lists;
        }
        if (nums.length==1){
            List<Integer> list=new ArrayList<>();
            list.add(nums[0]);
            lists.add(list);
            return lists;
        }
        test(nums,lists,0);
        return lists;
    }

    public static void test(int[]nums,List<List<Integer>> list,int index){
        List<Integer> list1=new ArrayList<>();
        List<Integer> list2=new ArrayList<>();
        if ((index+2)==nums.length){
            for (int i = 0; i < index; i++) {
                list1.add(nums[i]);
                list2.add(nums[i]);
            }
            list1.add(nums[index]);
            list1.add(nums[nums.length-1]);
            if (nums[index]==nums[nums.length-1]){
                list.add(list1);
            }else {
                list2.add(nums[nums.length-1]);
                list2.add(nums[index]);
                list.add(list1);
                list.add(list2);
            }
        }else {
            for (int i=0;i<(nums.length-index);i++){
                if (i!=0){
                    //判断是否跟其他数字相同
                    int flag=0;
                    for (int j=index;j<index+i;j++){
                        if (nums[index+i]==nums[j]){
                            flag=1;
                        }
                    }
                    if (flag==0){
                        //交换数组位置
                        int temp=nums[index];
                        nums[index]=nums[index+i];
                        nums[index+i]=temp;
                        int a=index+1;
                        test(nums,list,a);
                        //交换到原来得位置
                        int temp1=nums[index+i];
                        nums[index+i]=nums[index];
                        nums[index]=temp1;
                    }

                }else {
                    int a=index+1;
                    test(nums,list,a);
                }
            }
        }
    }

    /**
     * @param S: A set of numbers.
     * @return: A list of lists. All valid subsets.
     */
    public static List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> lists=new ArrayList<>();
        List<Integer> list=new ArrayList<>();
        if (nums==null || nums.length==0){
            lists.add(list);
            return lists;
        }
        Queue<List<Integer>> queue=new LinkedList<>();
        //记录下标的list
        Queue<Integer> indexList=new LinkedList<>();
        /**
         * TODO 对nums排序,这里运用我自己写的快速排序来进行解决
         *
         */
        quickSelect(0, nums.length - 1, nums);
//        Arrays.sort(nums);
//        queue.offer(); 添加
//        queue.poll(); 取出

        queue.offer(list);

        while (queue.size()>0){
            List<Integer> poll = queue.poll();
            lists.add(poll);
            Integer search;
            if (indexList.size()==0){
                search=-1;
            }else {
                search = indexList.poll();
            }
            //用二分查找法查找下标
//            int search=0;
//            if (poll.size()==0){
//               search=-1;
//            }else {
//                search = binarySearch(nums, poll.get(poll.size() - 1));
//            }

            for (int i=search+1;i<nums.length;i++){
                List<Integer> aa=new ArrayList<>();
                //如果前面有重复的就不取
                int flag=0;
                for (int j=search+1;j<i;j++){
                    if (nums[i]==nums[j]){
                        flag=1;
                    }
                }
                if (flag==0){
                    aa.addAll(poll);
                    aa.add(nums[i]);
                    queue.offer(aa);
                    //记录下标
                    indexList.offer(i);
                }


            }
        }

        return lists;
    }

    /**
     * @param S: A set of numbers.
     * @return: A list of lists. All valid subsets.
     */
    public static List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> lists=new ArrayList<>();
        List<Integer> list=new ArrayList<>();
        if (nums==null || nums.length==0){
            lists.add(list);
            return lists;
        }
        Queue<List<Integer>> queue=new LinkedList<>();
        //记录下标的list
        Queue<Integer> indexList=new LinkedList<>();
        /**
         * TODO 对nums排序,这里运用我自己写的快速排序来进行解决
         *
         */
        quickSelect(0, nums.length - 1, nums);
//        Arrays.sort(nums);
//        queue.offer(); 添加
//        queue.poll(); 取出

        queue.offer(list);
        while (queue.size()>0){
            List<Integer> poll = queue.poll();
            lists.add(poll);

            Integer search;
            if (indexList.size()==0){
                search=-1;
            }else {
                search = indexList.poll();
            }
            for (int i=search+1;i<nums.length;i++){
                List<Integer> aa=new ArrayList<>();
                aa.addAll(poll);
                aa.add(nums[i]);
                queue.offer(aa);
                //记录下标
                indexList.offer(i);
            }
        }
        return lists;
    }

    /**
     * 快速排序
     * @param left
     * @param right
     * @param nums
     * @param n
     */
    public static void quickSelect(int start, int end, int[] nums) {
        int pivot = nums[(start + end) / 2];
        int left = start;
        int right = end;
        while (left <= right) {
            while (nums[left] < pivot && left <= right) {
                left++;
            }
            while (nums[right] > pivot && left <= right) {
                right--;
            }
            if (left <= right) {
                //交换
                int swap = 0;
                swap = nums[right];
                nums[right] = nums[left];
                nums[left] = swap;
                left++;
                right--;
            }
        }
        if (right>start) {
            quickSelect(start, right, nums);
        }

        if (left<end) {
            quickSelect(left, end, nums);
        }


    }


    /**
     * @param n an integer
     * @return a list of Map.Entry<sum, probability>
     */
    public static List<Map.Entry<Integer, Double>> dicesSum(int n) {
        // Write your code here
        // Ps. new AbstractMap.SimpleEntry<Integer, Double>(sum, pro)
        // to create the pair
        List<Map.Entry<Integer,Double>> list=new ArrayList<>();
        int min=n;
        int max=n*6;
        int length=max-min+1;
        //总共有多少次
        int f=n;
        int countMy=1;
        while (f>0){
            countMy=countMy*6;
            f--;
        }
        //中间的数
        int media=length/2+min;
        //先判断奇偶
        if ((length&1)==1){
            //奇数 有一个最大值 求最大概率
            int count=1;
            int t=n;
            while (t>1){
                count=count*t;
                t--;
            }
            count=count*3;
            for (int i=0;i<length;i++){
                int aa=media-min-i;
                if (aa<0){
                    aa=aa/(-1);
                }
                int i1 = count -aa * (n-1);
                //格式化小数
                DecimalFormat df = new DecimalFormat("0.00");
                //返回的是String类型
                String num = df.format((float)i1/countMy);
                AbstractMap.SimpleEntry<Integer, Double> entry = new AbstractMap.SimpleEntry<>(min+i,new Double(num));
                list.add(entry);
            }

        }else {
            //偶数 有两个最大值
            int count=1;
            int t=n;
            while (t>1){
                count=count*t;
                t--;
            }
            count=count*3;
            for (int i=0;i<(length/2-1);i++){

            }
        }
        return list;
    }

    /**
     * @param s1: A string
     * @param s2: A string
     * @param s3: A string
     * @return: Determine whether s3 is formed by interleaving of s1 and s2
     */
    public static boolean isInterleave(String s1, String s2, String s3) {
        // write your code here
        char[] s1Array = s1.toCharArray();
        char[] s2Array = s2.toCharArray();
        char[] s3Array = s3.toCharArray();
        int i=0;
        int j=0;
        int k=0;
        while (k<s3Array.length){


            if (s3Array[k]==s1Array[i]){
                i++;

            }else {
                if (s3Array[k]==s2Array[j]){
                    j++;
                }
            }
            k++;
        }
        return false;
    }

}
