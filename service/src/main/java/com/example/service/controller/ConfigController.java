package com.example.service.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *@author zs
 *@date 2019/10/25 10:34
 *@Company:成都壹柒互动科技有限公司
 *@description TODO 启动配置管理
 */
@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {

    @Value("${zs.test}")
    private String test;

    @RequestMapping("/get")
    public String get() {
        return test;
    }
}
